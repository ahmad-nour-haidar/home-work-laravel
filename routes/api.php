<?php

use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/users/getAllUsersNames',[UserController::class ,'getAllUsersNames']);
Route::get('/users/getAllUsers',[UserController::class ,'getAllUsers']);
Route::post('/users/create',[UserController::class ,'create']);

Route::middleware('check_token')
    ->controller(ProductController::class)
    ->prefix('product')
    ->group(function (){
        Route::post('/add','add');
        Route::post('/edit','edit');
        Route::delete('/delete','delete');
    });
Route::get('/product/get-all',[ProductController::class ,'getAll']);


