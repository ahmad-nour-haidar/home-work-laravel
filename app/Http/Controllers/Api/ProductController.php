<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    function getAll(){
        $data = Product::all();

        return $this->sendResponse($data, self::$success);
    }


    function add(Request $request)
    {

        if (!isset($request['name'])) {
            return $this->sendResponse(null, 'name not sent');
        }
        if (!isset($request['desc'])) {
            return $this->sendResponse(null, 'desc not sent');
        }
        if (!isset($request['owner_email'])) {
            return $this->sendResponse(null, 'owner_email not sent');
        }

        $name = $request->input('name');
        $desc = $request->input('desc');
        $ownerEmail = $request->input('owner_email');
        $input = [
            'name' => $name,
            'desc' => $desc,
            'owner_email' => $ownerEmail,
        ];
        $product = Product::create($input);
        return $this->sendResponse($product, self::$success);
    }

    function edit(Request $request)
    {

        if (!isset($request['id'])) {
            return $this->sendResponse(null, 'id not sent');
        }
        if (!isset($request['owner_email'])) {
            return $this->sendResponse(null, 'owner_email not sent');
        }
        if (!isset($request['name'])) {
            return $this->sendResponse(null, 'name not sent');
        }
        if (!isset($request['desc'])) {
            return $this->sendResponse(null, 'desc not sent');
        }
        $id = $request->input('id');
        $name = $request->input('name');
        $desc = $request->input('desc');
        $ownerEmail = $request->input('owner_email');
        $product = Product::find($id);
        if (!$product) {
            return $this->sendResponse(null, 'product not found');
        }
        if ($product->owner_email != $ownerEmail) {
            return $this->sendResponse(null, 'owner_email not correct');
        }
        $input = [
            'name' => $name,
            'desc' => $desc,
        ];
        $product->update($input);
        return $this->sendResponse($product, self::$success);
    }

    function delete(Request $request)
    {
//        echo $id;
//        if (!isset($request['id'])) {
//            return $this->sendResponse(null, 'id not sent');
//        }
//        $id = $request->input('id');
        $id = $request->query('id');
        $product = Product::find($id);
        if (!$product) {
            return $this->sendResponse(null, 'product not found');
        }
        $product->delete();
        return $this->sendResponse(null, self::$success);
    }
}
