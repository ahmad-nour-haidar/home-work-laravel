<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    function getAllUsersNames()
    {
        $data = User::select('name')->get();
        return $this->sendResponse($data, self::$success);
    }

    function getAllUsers()
    {
        $data = User::all();
        return $this->sendResponse($data, self::$success);
    }

    function create(Request $request)
    {
        if (!isset($request['email'])) {
            return $this->sendResponse(null, 'email not sent');
        }
        if (!isset($request['phone'])) {
            return $this->sendResponse(null, 'phone not sent');
        }
        if (!isset($request['name'])) {
            return $this->sendResponse(null, 'name not sent');
        }
        if (!isset($request['password'])) {
            return $this->sendResponse(null, 'password not sent');
        }
        $email = $request->input('email');
        $phone = $request->input('phone');
        $name = $request->input('name');
        $password = $request->input('password');
        $input = [
            'email' => $email,
            'phone' => $phone,
            'name' => $name,
            'password' => $password,
        ];

        $check = User::where('email', $email)
            ->orWhere('phone', $phone)
            ->first();
        if ($check) {
            return $this->sendResponse(null, 'email or phone already taken');
        }
        $user = User::create($input);
        return $this->sendResponse($user, self::$success);
    }

}
