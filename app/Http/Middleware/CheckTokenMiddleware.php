<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */

    private $allowedEmails = [
        'SamerSharaf@gmail.com'
    ];

    public function handle(Request $request, Closure $next)
    {
        $error = false;
        if (!$request->hasHeader('x-ite-token')) {
            $error = true;
        }
        $token = $request->header('x-ite-token');
        try {
            $jsonStr = base64_decode($token);
            $jsonPayload = json_decode($jsonStr, true);
            if (!$jsonPayload) {
                $error = true;
            }
            if (!isset($jsonPayload['email'])) {
                $error = true;
            }
            if (!in_array($jsonPayload['email'], $this->allowedEmails)) {
                $error = true;
            }
        } catch (Exception $e) {
            $error = true;
        }
        if ($error) {
            return response()->json([
                'status' => 501,
                'message' => 'Invalid token',
            ]);
        }
        return $next($request);
    }
}
